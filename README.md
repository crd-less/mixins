# Less Mixins and Utilities

This package provides a set of Less CSS Mixins utilities:

### Required ```less``` files:

At the very beginning of your main styles ```less``` files add:

```less
@import "all";
```

That will include all the necessary files for the mixins and utilities to work. Otherwise, you can include just the necessary files, according to your needs.

The files included in this package are:

- ```variables.less```, all the variables in order to all the mixins and utilities to work
- ```mixins.less```, just the mixins (requires ```variables.less```)
- ```utils.less```, all the utilities (requires ```variables.less``` and ```mixins.less```)
- ```animations.less```, all the animations utilities (requires ```variables.less``` and ```mixins.less```)

## CSS Reset

Applies a global ```css``` reset [http://meyerweb.com/eric/tools/css/reset/](CSS Reset).

### Example

Add the folowing mixin at the top of your ```less``` file

```less
.css-reset();
```

Output:

```css
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, ruby, section, summary,
time, mark, audio, video {
	margin: 0;
	padding: 0;
	border: 0;
	font-size: 100%;
	font: inherit;
	vertical-align: baseline;
}
article, aside, details, figcaption, figure,
footer, header, hgroup, menu, nav, section {
	display: block;
}
body {
	line-height: 1;
}
ol, ul {
	list-style: none;
}
blockquote, q {
	quotes: none;
}
blockquote:before, blockquote:after,
q:before, q:after {
	content: '';
	content: none;
}
table {
	border-collapse: collapse;
	border-spacing: 0;
}
```

## Transitions

Applies any desired ```css``` transition.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
... | no default | Set of given transitions to apply, separated by comma.

### Example

Add the following mixin in the desired ```css``` rule to apply the ```transition``` property.

```less
a {
	.transition(color 0.25s linear);
}

// Multiple transitions
a {
	.transition(~'color 0.25s linear, font-size 1s linear');
}
```

Output:

```css
a {
	transition: color 0.25s linear;
}

a {
	transition: color 0.25s linear, font-size 1s linear;
}
```

*All the specific vendor prefixes will be applies if you use the Npm Less Auto-Prefix Plug-in.*

## Border Radius

Applies any desired ```css``` border radius to the selector.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
... | no default | Border radius values to apply.

### Example

Add the following mixin in the desired ```css``` rule to apply the ```border-radius``` property.

```less
div {
	.border-radius(10px);
}

// Different for each corner
div {
	.border-radius(10px 0px 10px 0px);
}
```

Output:

```css
div {
	border-radius: 10px;
}

div {
	border-radius: 10px 0 10px 0;
}
```

*All the specific vendor prefixes will be applies if you use the Npm Less Auto-Prefix Plug-in.*

## Box Shadow

Applies a box shadow ```css``` property, use the ```.box-shadow()``` mixing in any of your ```css``` rules.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
... | no default | Box shadow values to apply.

### Example

Add the following mixin in the desired ```css``` rule to apply the ```box-shadow``` property.

```less
div {
	.box-shadow(5px 5px 10px black);
}
```

Output:

```css
div {
	box-shadow: 5px 5px 10px black;
}
```

*All the specific vendor prefixes will be applies if you use the Npm Less Auto-Prefix Plug-in.*

## Vertical Alignment

Adds a set of ```css``` properties, to align any nested element, using absolute position and ```css``` transform.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@position | absolute | Position to apply to the element.
@offset | 50% | Centering offset (> 50% will bring the element bottom; < 50% will bring the element up).

### Example

Add the following mixin in the desired ```css``` rule to center the element vertically. Note that the element must be nested into another block element, with ```position: relative```.

```less
.align {
	.vertical-align();
}
```

Output:

```css
.align {
	position: absolute;
	top: 50%;
	transform: translateY(-50%);
}
```

*All the specific vendor prefixes will be applies if you use the Npm Less Auto-Prefix Plug-in.*

## Center Block

Adds a set of ```css``` properties, to align horizontally align a block element using automatic margins.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@display | block | Type of display for the element.

### Example

Add the following mixin in the desired ```css``` rule to center the element horizontally.

```less
.centered {
	.center-block();
}
```

Output:

```css
.centered {
	display: block;
	margin-left: auto;
	margin-right: auto;
}
```

## Font Faces

Sets a ```font-face```, ```font-weight``` and ```font-size``` ```css``` properties to any selector.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@font-family | @default-font-face | Font face to apply to the element.
@font-weight | no default | Font weight to apply to the element.
@font-size | no default | Font size to apply to the element.

### Example

Add the following mixin in the desired ```css``` rule to set its font face, size and weight.

```less
.styled {
	.font-face('open-sans', sans-serif; 400; 1.2em);
}
```

Output:

```css
.styled {
	font-family: 'open-sans', sans-serif;
	font-weight: 400;
	font-size: 1.2em;
}
```

## Tuncate Text

Truncate text in any block element using ```text-overflow``` ```css``` property.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@type | ellipsis | Text overflow type.
@white-space | nowrap | White-space tratment.

### Example

Add the following mixin in the desired ```css``` rule to truncate text.

```less
.truncated {
	.truncate-text();
}
```

Output:

```css
.truncated {
	overflow: hidden;
	text-overflow: ellipsis;
	white-space: nowrap;
}
```

## Multiply Styles

Repeat any custom ```css``` class that changes any numeric-like property, increasing its value by a defined number.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@property | no default | Property that the class will increase or decrease its value.
@name | no default | Name to use for the class; the name will be appended with a dash and the numeric representation of the step.
@current | 0px | Current or initial value for the property.
@offset | 5px | Offset o value to increase on each step.
@max | 50px | Maximum value for the property.

### Example

Use the following mixin to create multiple ```css``` classes to set any element ```margin-bottom``` in steps of 5 pixels.

```less
.multiply(margin-bottom; m; 0; 5px; 20px);
```

Output:

```css
.m-0 {
	margin-bottom: 0px;
}
.m-5 {
	margin-bottom: 5px;
}
.m-10 {
	margin-bottom: 10px;
}
.m-15 {
	margin-bottom: 15px;
}
.m-20 {
	margin-bottom: 20px;
}
```

## Classes Defined from a List

Defines any number of ```css``` classes using a list as a set of name and values, for any defined ```css``` property.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@property | no default | Property that the class will use to set its value from the list.
@name | no default | Name to use for the class; the name will be appended with a name (key) defined on the list.
@list | no default | List containg sets of key and value, that will be assigned as name and value for the property.
@offset | 1 | Current pointer for the list.

### Example

Use the following mixin to create multiple ```css``` classes to set any element ```background-color```, with the colors defined on a list.

```less
@list :
	black #000,
	white #fff;
	
.define-from-list(background-color; bg; @list);
```

Output:

```css
.bg-black {
	background-color: #000;
}
.bg-white {
	background-color: #fff;
}
```

## Input Placeholder Styles

Add text styles to any input placeholder using all the vendor specific ```css``` selectors.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@rules | no default | Sets of ```css``` properties to be applied to the placeholder style.
@selector | input | The selector to apply all the rules.

### Example

Use the following mixin to style the placeholder for any text input.

```less
.placeholder({ color: grey; }; input[type="text"]);
```

Output:

```css
input[type="text"]::-webkit-input-placeholder {
	color: grey;
}
input[type="text"]::-moz-placeholder {
	color: grey;
}
input[type="text"]:-ms-input-placeholder {
	color: grey;
}
input[type="text"]:-moz-placeholder {
	color: grey;
}
```

## Equal Heigh Elements

Makes all the block elements contained in a main element (different Bootrsrap columns, contained in a row), get the height of the taller element. The taller element should be at the very begining (otherwise, use pull or push classes to pull or push the taller row to the desired position - for bootstrap -, or change the elements order or the ```@direction``` property).

#### For Bootsrap

In order to apply background colors to the nested elements (on any column), the columns should have ```height: 100%;```.

To continue with a normal Bootstrap build method inside the columns affected by the mixin, the reset method should be applied.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@direction | row | Direction for the height to apply. [http://www.w3schools.com/cssref/css3_pr_flex-direction.asp](http://www.w3schools.com/cssref/css3_pr_flex-direction.asp)
@align | center | Column alignment. [http://www.w3schools.com/cssref/css3_pr_align-items.asp](http://www.w3schools.com/cssref/css3_pr_align-items.asp)
@justify | flex-start | Content justify. [http://www.w3schools.com/cssref/css3_pr_justify-content.asp](http://www.w3schools.com/cssref/css3_pr_justify-content.asp)
@selector | ~'> [class^="col-"]', ~'> [class*=" col-"]' | Selector or list of selectors for the rules to be applied.

### Example

Apply the following mixing to any row, to make the nested columns get equal height.

```less
.row {
	.row-equal-height();
}
```

Output:

```css
.row {
	display: flex;
	flex-direction: row;
}
.row > [class^="col-"],
.row > [class*=" col-"] {
	display: flex;
	align-items: center;
	justify-content: flex-start;
}
```

*All the specific vendor prefixes will be applies if you use the Npm Less Auto-Prefix Plug-in.*

## Equal Heigh Elements Reset

Resets all the properties changes by ```.row-equal-height()``` mixin, to its original state.

#### For Bootsrap

In order for the row to work as usual, add ```width: 100%;``` property to the reseted elements.

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@selector | ~'> [class^="col-"]', ~'> [class*=" col-"]' | Selector or list of selectors for the rules to be applied.

### Example

Apply the following mixing to any row after applying the ```.row-equeal-height()``` mixin, to reset all the nested rows and columns to its original states.

```less
.row {
	.row-equal-height();
	.row {
		width: 100%;
		.row-equal-height-reset();
	}
}
```

Output:

```css
.row {
	display: flex;
	flex-direction: row;
}
.row > [class^="col-"],
.row > [class*=" col-"] {
	display: flex;
	align-items: center;
	justify-content: flex-start;
}
.row .row {
	width: 100%;
}
.row .row > [class^="col-"],
.row .row > [class*=" col-"] {
	display: flex;
	align-items: stretch;
	justify-content: flex-start;
}
```

*All the specific vendor prefixes will be applies if you use the Npm Less Auto-Prefix Plug-in.*

## Gradients

Apply a ```css``` gradient (```svg```, ```css``` and a fallback for old browsers).

The mixin accepts the following parameters:

Param | Default | Description
------ | ---- | -------
@start | no default | Gradient start color.
@end | no default | Gradient end color.
@direction | right | Direction for the gradient.
@fallback | @start | Fallback color for old browsers.

### Example

Apply the following mixing to any ```css``` selector to apply a gradient overlay to any element.

```less
.gradient {
	.gradient(#ffffff; #000000);
}
```

Output:

```css
.gradient {
	background-color: #ffffff;
	background-image: url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmZmZmZiIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiMwMDAwMDAiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+);;
	background: linear-gradient(to right, #ffffff 0%, #000000 100%);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffff', endColorstr='#000000',GradientType=0);
}
```

*All the specific vendor prefixes will be applies if you use the Npm Less Auto-Prefix Plug-in.*

## Configuration file

A main configuration file is provided (```variables.less```) where every aspect of the mixins, utilities and animation utilities can be configured.

The available variables are:

Option | mixins.less | utils.less | animations.less | Default | Description
------ | ------ | ----- | ---------- | ------- | -----------
@screen-xs-max | --- | Required | --- | 767px | Bootstrap configuration.
@screen-sm-min | --- | Required | --- | 768px | Bootstrap configuration.
@screen-md-min | --- | Required | --- | 992px | Bootstrap configuration.
@screen-lg-min | --- | Required | --- | 1200px | Bootstrap configuration.
@default-font-face | Required | --- | --- | "Roboto", sans-serif | Defalut font face.
@default-font-weight | Required | --- | --- | 400 | Defautl font weight.
@margin-offset | --- | Required | --- | 5px | Margin offset for margins definitions.
@margin-max-value | --- | Required | --- | 50px | Max value for margins definitions.
@margin-offset-xs | --- | Required | --- | 5px | Margin offset for margins definitions (xs).
@margin-max-value-xs | --- | Required | --- | 50px | Max value for margins definitions (xs).
@margin-offset-sm | --- | Required | --- | 5px | Margin offset for margins definitions (sm).
@margin-max-value-sm | --- | Required | --- | 50px | Max value for margins definitions (sm).
@margin-offset-md | --- | Required | --- | 5px | Margin offset for margins definitions (md).
@margin-max-value-md | --- | Required | --- | 50px | Max value for margins definitions (md).
@margin-offset-lg | --- | Required | --- | 5px | Margin offset for margins definitions (lg).
@margin-max-value-lg | --- | Required | --- | 50px | Max value for margins definitions (lg).
@padding-offset | --- | Required | --- | 5px | Margin offset for paddings definitions.
@padding-max-value | --- | Required | --- | 50px | Max value for paddings definitions.
@padding-offset-xs | --- | Required | --- | 5px | Margin offset for paddings definitions (xs).
@padding-max-value-xs | --- | Required | --- | 50px | Max value for paddings definitions (xs).
@padding-offset-sm | --- | Required | --- | 5px | Margin offset for paddings definitions (sm).
@padding-max-value-sm | --- | Required | --- | 50px | Max value for paddings definitions (sm).
@padding-offset-md | --- | Required | --- | 5px | Margin offset for paddings definitions (md).
@padding-max-value-md | --- | Required | --- | 50px | Max value for paddings definitions (md).
@padding-offset-lg | --- | Required | --- | 5px | Margin offset for paddings definitions (lg).
@padding-max-value-lg | --- | Required | --- | 50px | Max value for paddings definitions (lg).
@animation-duration | --- | --- | Required | 0.25s | Default animation duration.
@animation-delay-start | --- | --- | Required | 0.25s | Start delay for animations.
@animation-delay-offset | --- | --- | Required | 0.25s | Delay offset for animations.
@animation-delay-max-value | --- | --- | Required | 2.5s | Max value for delays definitions.

## More Examples

More helpers can be defined using the multiply mixin.

#### Background colors

This can be applyed for text colors (for example, using a ```txt``` prefix instead of ```bg```);

```less
@bg-colors:
	transparent transparent,
	black #000000,
	white #ffffff;
	
.define-from-list(background-color; bg; @bg-colors);
```

Output:

```css
.bg-transparent {
	background-color: transparent;
}
.bg-black {
	background-color: #000000;
}
.bg-white {
	background-color:#ffffff;
}
```

#### Font weights definition

```less
@font-weights :
	light 300,
	regular 400,
	bold 700;
	
.define-from-list(font-weight; txt; @font-weights);
```

Output:

```css
.txt-light {
	font-weight: 300;
}
.txt-regular {
	font-weight: 400;
}
.txt-bold {
	font-weight: 700;
}
```

## Browser Support

Less Mixin Utilities packs a custom ```browserlist``` file to use with the Npm Less compiles [https://www.npmjs.com/package/less](NPM Less Compiler) and auto-prefix npm plug-in for Less [https://www.npmjs.com/package/less-plugin-autoprefix](Less plug-in auto-prefix). Less Mixin Utilities will not handle browser specific vendor property prefixes.

## License

Copyright (c) 2016 Luciano Giordano

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
